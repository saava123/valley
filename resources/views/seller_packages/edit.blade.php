@extends('layouts.back-end.app')

@section('content')
<div class="aiz-titlebar mt-2 mb-3">
    <h5 class="mb-0 h6">{{\App\CPU\translate('Update Package Information')}}</h5>
</div>

<div class="col-lg-10 mx-auto">
    <div class="card">
        <div class="card-body p-0">

            <form class="p-4" action="{{ route('seller_packages.update', $seller_package->id) }}" method="POST">
                <input type="hidden" name="_method" value="PATCH">
            	@csrf
                <div class="form-group row">
                    <label class="col-sm-2 col-from-label" for="name">{{\App\CPU\translate('Package Name')}}</label>
                    <div class="col-sm-10">
                        <input type="text" placeholder="{{\App\CPU\translate('Name')}}" value="{{ $seller_package->name }}"
                               id="name" name="name" class="form-control" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-from-label" for="amount">{{\App\CPU\translate('Amount')}}</label>
                    <div class="col-sm-10">
                        <input type="number" min="0" step="0.01" placeholder="{{\App\CPU\translate('Amount')}}"
                               value="{{ $seller_package->amount }}" id="amount" name="amount" class="form-control" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-from-label" for="product_upload_limit">{{\App\CPU\translate('Product Upload Limit')}}</label>
                    <div class="col-sm-10">
                        <input type="number" min="0" step="1" placeholder="{{\App\CPU\translate('Product Upload Limit')}}"
                               value="{{ $seller_package->product_upload_limit }}" id="product_upload_limit"
                               name="product_upload_limit" class="form-control" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-from-label" for="duration">{{\App\CPU\translate('Duration')}}</label>
                    <div class="col-sm-10">
                        <input type="number" min="0" step="1" placeholder="{{\App\CPU\translate('Validity in number of days')}}"
                               value="{{ $seller_package->duration }}" id="duration" name="duration" class="form-control" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="signinSrEmail">{{\App\CPU\translate('Package Logo')}}</label>
                    <div class="col-md-10">
                        <div class="input-group" data-toggle="aizuploader" data-type="image" data-multiple="false">
                            <div class="input-group-prepend">
                                <div class="input-group-text bg-soft-secondary font-weight-medium">{{ \App\CPU\translate('Browse')}}</div>
                            </div>

                            <div class="custom-file" style="text-align: left">
                                <input type="file" name="logo" id="customFileUpload" value="{{ $seller_package->logo }}" class="custom-file-input"
                                       accept=".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|image/*">
                                <label class="custom-file-label" for="customFileUpload">{{\App\CPU\translate('choose')}}
                                    {{\App\CPU\translate('logo')}}</label>
                            </div>

                        </div>
                        <div class="col-md-6 mb-2">
                            <center>
                                <img style="border-radius: 10px; max-height:200px;" id="viewer"
                                     src="{{asset('storage/app/public/packages')}}/{{$seller_package->logo}}" alt="package image"/>
                            </center>
                        </div>
                    </div>
                </div>
                <div class="form-group mb-0 text-right">
                    <button type="submit" class="btn btn-sm btn-primary">{{\App\CPU\translate('Save')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!--modal-->
@include('shared-partials.image-process._image-crop-modal',['modal_id'=>'brand-image-modal','width'=>1000,'margin_left'=>'-53%'])
<!--modal-->
@endsection

@push('script')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#viewer').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#customFileUpload").change(function () {
            readURL(this);
        });
    </script>

@endpush
