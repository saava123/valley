@extends('layouts.back-end.app')

@section('content')
    <div class="content container-fluid">
        <!-- Page Heading -->
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">{{ \App\CPU\translate('Dashboard')}}</a></li>
                <li class="breadcrumb-item" aria-current="page">{{ \App\CPU\translate('Attribute')}}</li>
            </ol>
        </nav>
        <div class="aiz-titlebar mt-2 mb-3">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <h1 class="h3">{{\App\CPU\translate('All Seller Packages')}}</h1>
                </div>
                <div class="col-md-6 text-md-right">
                    <a href="{{ route('seller_packages.create') }}" class="btn btn-circle btn-info">
                        <span>{{\App\CPU\translate('Add New Package')}}</span>
                    </a>
                </div>
            </div>
        </div>


        <div class="row">
            @foreach ($seller_packages as $key => $seller_package)
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="card">
                        <div class="card-body text-center">
                            <img alt="{{ \App\CPU\translate('Package Logo')}}" src="{{asset('storage/app/public/packages')}}/{{$seller_package->logo}}"
                                 class="mw-100 mx-auto mb-4" height="150px">
                            <p class="mb-3 h6 fw-600">{{ $seller_package->name }}</p>
                            <p class="h4">{{$seller_package->amount}}</p>
                            <p class="fs-15">{{\App\CPU\translate('Product Upload Limit') }}:
                                <b class="text-bold">{{$seller_package->product_upload_limit}}</b>
                            </p>
                            <p class="fs-15">{{\App\CPU\translate('Package Duration') }}:
                                <b class="text-bold">{{$seller_package->duration}} {{\App\CPU\translate('days')}}</b>
                            </p>
                            <div class="mar-top">
                                <a href="{{route('seller_packages.edit', ['id'=>$seller_package->id, 'lang'=>env('DEFAULT_LANGUAGE')] )}}"
                                   class="btn  btn-info">{{\App\CPU\translate('Edit')}}</a>
                                <a href="#" data-href="{{route('seller_packages.destroy', $seller_package->id)}}"
                                   class="btn  btn-danger confirm-delete">{{\App\CPU\translate('Delete')}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

@endsection

@section('modal')
    @include('admin-views.modals.delete')
@endsection
