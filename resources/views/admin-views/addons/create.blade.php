@extends('layouts.back-end.app')
@section('title', \App\CPU\translate('Addons'))
@push('css_or_js')
    <!-- Custom styles for this page -->
    {{-- <link href="{{asset('public/assets/back-end')}}/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet"> --}}
    {{-- <link href="{{asset('public/assets/back-end/css/croppie.css')}}" rel="stylesheet"> --}}
@endpush

@section('content')
    <div class="content container-fluid">
        <!-- Page Heading -->
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">{{ \App\CPU\translate('Dashboard')}}</a></li>
                <li class="breadcrumb-item" aria-current="page">{{ \App\CPU\translate('Addons')}}</li>
            </ol>
        </nav>

        <!-- Content Row -->
        <div class="row">
            <div class="col-lg-7 mx-auto">
                <div class="card">
                    <div class="card-header">
                        <h5 class="mb-0 h6">{{ \App\CPU\translate('Install/Update Addon')}}</h5>
                    </div>
                    <form class="form-horizontal" action="{{ route('admin.addons-manageraddons.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">

                            <div class="form-group row">
                                <label class="col-sm-3 col-from-label" for="addon_zip">{{ \App\CPU\translate('Zip File')}}</label>
                                <div class="col-sm-9">
                                    <div class="custom-file">
                                        <label class="custom-file-label">
                                            <input type="file" id="addon_zip" name="addon_zip"  class="custom-file-input" required>
                                            <span class="custom-file-name">{{ \App\CPU\translate('Choose file') }}</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mb-0 text-right">
                                <button type="submit" class="btn btn-primary">{{\App\CPU\translate('Install/Update')}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection


@push('script')

    <script>

        // Call the dataTables jQuery plugin


    </script>
@endpush
