@extends('layouts.back-end.app')

@section('title', \App\CPU\translate('Cities'))

@push('css_or_js')

@endpush

@section('content')

    <div class="content container-fluid">
        <div class="aiz-titlebar text-left mt-2 mb-3">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <h1 class="h3">{{\App\CPU\translate('All cities')}}</h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7">
                <div class="card">
                    <form class="" id="sort_cities" action="" method="GET">
                        <div class="card-header row gutters-5">
                            <div class="col text-center text-md-left">
                                <h5 class="mb-md-0 h6">{{ \App\CPU\translate('Cities') }}</h5>
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control" id="sort_city" name="sort_city" @isset($sort_city) value="{{ $sort_city }}" @endisset placeholder="{{ \App\CPU\translate('Type city name & Enter') }}">
                            </div>
                            <div class="col-md-4">
                                <select class="form-control aiz-selectpicker" data-live-search="true" id="sort_state" name="sort_state">
                                    <option value="">{{ \App\CPU\translate('Select State') }}</option>
                                    @foreach ($states as $state)
                                        <option value="{{ $state->id }}" @if ($sort_state == $state->id) selected @endif {{$sort_state}}>
                                            {{ $state->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-1">
                                <button class="btn btn-primary" type="submit">{{ \App\CPU\translate('Filter') }}</button>
                            </div>
                        </div>
                    </form>
                    <div class="card-body">
                        <table class="table aiz-table1 mb-0">
                            <thead>
                            <tr>
                                <th data-breakpoints="lg">#</th>
                                <th>{{\App\CPU\translate('Name')}}</th>
                                <th>{{\App\CPU\translate('State')}}</th>
                                <th>{{\App\CPU\translate('Area Wise Shipping Cost')}}</th>
                                <th>{{\App\CPU\translate('Show/Hide')}}</th>
                                <th data-breakpoints="lg" class="text-right">{{\App\CPU\translate('Options')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($cities as $key => $city)
                                <tr>
                                    <td>{{ ($key+1) + ($cities->currentPage() - 1)*$cities->perPage() }}</td>
                                    <td>{{ $city->name }}</td>
                                    <td>{{ $city->state->name }}</td>
                                    <td>{{ $city->cost }}</td>
                                    <td>
                                        <label class="aiz-switch aiz-switch-success mb-0">
                                            <input onchange="update_status(this)" value="{{ $city->id }}" type="checkbox" <?php if($city->status == 1) echo "checked";?> >
                                            <span class="slider round"></span>
                                        </label>
                                    </td>
                                    <td class="text-right">
                                        <a class="btn btn-soft-primary btn-icon btn-circle btn-sm" href="{{ route('admin.business-settings.cities.edit', ['id'=>$city->id, 'lang'=>env('DEFAULT_LANGUAGE')]) }}" title="{{ \App\CPU\translate('Edit') }}">
                                            <i class="las la-edit"></i>
                                        </a>
                                        <a href="#delete-modal" class="btn btn-soft-danger btn-icon btn-circle btn-sm confirm-delete" data-href="{{route('admin.business-settings.cities.destroy', $city->id)}}" title="{{ \App\CPU\translate('Delete') }}">
                                            <i class="las la-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="aiz-pagination">
                            {{ $cities->appends(request()->input())->links() }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="card">
                    <div class="card-header">
                        <h5 class="mb-0 h6">{{ \App\CPU\translate('Add New city') }}</h5>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('admin.business-settings.cities.store') }}" method="POST">
                            @csrf
                            <div class="form-group mb-3">
                                <label for="name">{{\App\CPU\translate('Name')}}</label>
                                <input type="text" placeholder="{{\App\CPU\translate('Name')}}" name="name" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label for="country">{{\App\CPU\translate('State')}}</label>
                                <select class="select2 form-control aiz-selectpicker" name="state_id" data-toggle="select2" data-placeholder="Choose ..." data-live-search="true">
                                    @foreach ($states as $state)
                                        <option value="{{ $state->id }}">{{ $state->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group mb-3">
                                <label for="name">{{\App\CPU\translate('Cost')}}</label>
                                <input type="number" min="0" step="0.01" placeholder="{{\App\CPU\translate('Cost')}}" name="cost" class="form-control" required>
                            </div>
                            <div class="form-group mb-3 text-right">
                                <button type="submit" class="btn btn-primary">{{\App\CPU\translate('Save')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

    @section('modal')
        @include('admin-views.modals.delete')
        @endsection
@endsection

@push('script')
    <!-- Page level custom scripts -->
    <script src="{{ asset('public/assets/select2/js/select2.min.js')}}"></script>
    <script>
        $(".js-example-theme-single").select2({
            theme: "classic"
        });

        $(".js-example-responsive").select2({
            width: 'resolve'
        });
    </script>

    <script>
        $('#add').on('click', function () {
            var name = $('#name').val();
            var symbol = $('#symbol').val();
            var code = $('#code').val();
            var exchange_rate = $('#exchange_rate').val();
            if (name == "" || symbol == "" || code == "" || exchange_rate == "") {
                alert('{{\App\CPU\translate('All input field is required')}}');
                return false;
            } else {
                return true;
            }
        });
        $(document).on('change', '.status', function () {
            var id = $(this).attr("id");
            if ($(this).prop("checked") == true) {
                var status = 1;
            } else if ($(this).prop("checked") == false) {
                var status = 0;
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{route('admin.business-settings.states.updateStatus')}}",
                method: 'POST',
                data: {
                    id: id,
                    status: status
                },
                success: function (response) {
                    if (response.status === 1) {
                        toastr.success(response.message);
                    } else {
                        toastr.error(response.message);
                    }
                    location.reload();
                }
            });
        });
    </script>
@endpush
