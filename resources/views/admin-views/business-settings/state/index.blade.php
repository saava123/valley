@extends('layouts.back-end.app')

@section('title', \App\CPU\translate('States'))

@push('css_or_js')

@endpush

@section('content')

    <div class="content container-fluid">

        <div class="aiz-titlebar text-left mt-2 mb-3">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <h1 class="h3">{{\App\CPU\translate('All States')}}</h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7">
                <div class="card">
                    <form class="" id="sort_cities" action="" method="GET">
                        <div class="card-header row gutters-5">
                            <div class="col text-center text-md-left">
                                <h5 class="mb-md-0 h6">{{ \App\CPU\translate('States') }}</h5>
                            </div>
                            <div class="col-md-3">
                                <input type="text" class="form-control" id="sort_state" name="sort_state" @isset($sort_state) value="{{ $sort_state }}" @endisset placeholder="{{ \App\CPU\translate('Type state name') }}">
                            </div>
                            <div class="col-md-3">
                                <select class="form-control aiz-selectpicker" data-live-search="true" id="sort_country" name="sort_country">
                                    <option value="">{{ \App\CPU\translate('Select Country') }}</option>
                                    @foreach (\App\Model\Country::where('status', 1)->get() as $country)
                                        <option value="{{ $country->id }}" @if ($sort_country == $country->id) selected @endif {{$sort_country}}>
                                            {{ $country->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-1">
                                <button class="btn btn-primary" type="submit">{{ \App\CPU\translate('Filter') }}</button>
                            </div>
                        </div>
                    </form>
                    <div class="card-body">
                        <table class="table aiz-tablfe mb-0">
                            <thead>
                            <tr>
                                <th width="10%">#</th>
                                <th>{{\App\CPU\translate('Name')}}</th>
                                <th>{{\App\CPU\translate('Country')}}</th>
                                <th>{{\App\CPU\translate('Show/Hide')}}</th>
                                <th class="text-right">{{\App\CPU\translate('Action')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($states as $key => $state)
                                <tr>
                                    <td>{{ ($key+1) + ($states->currentPage() - 1)*$states->perPage() }}</td>
                                    <td>{{ $state->name }}</td>
                                    <td>{{ $state->country->name }}</td>
                                    <td>
                                        <label class="aiz-switch aiz-switch-success mb-0">
                                            <input  value="{{ $state->id }}" type="checkbox" <?php if($state->status == 1) echo "checked";?> >
                                            <span class="slider round"></span>
                                        </label>
                                    </td>
                                    <td class="text-right">
                                        <a class="btn btn-soft-primary btn-icon btn-circle btn-sm" href="{{ route('admin.business-settings.states.edit', $state->id) }}" title="{{ \App\CPU\translate('Edit') }}">
                                            <i class="las la-edit"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="aiz-pagination">
                            {{ $states->appends(request()->input())->links() }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="card">
                    <div class="card-header">
                        <h5 class="mb-0 h6">{{ \App\CPU\translate('Add New State') }}</h5>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('admin.business-settings.states.store') }}" method="POST">
                            @csrf
                            <div class="form-group mb-3">
                                <label for="name">{{\App\CPU\translate('Name')}}</label>
                                <input type="text" placeholder="{{\App\CPU\translate('Name')}}" name="name" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label for="country">{{\App\CPU\translate('Country')}}</label>
                                <select class="select2 form-control aiz-selectpicker" name="country_id" data-toggle="select2" data-placeholder="Choose ..." data-live-search="true">
                                    @foreach (\App\Model\Country::where('status', 1)->get() as $country)
                                        <option value="{{ $country->id }}">
                                            {{ $country->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group mb-3 text-right">
                                <button type="submit" class="btn btn-primary">{{\App\CPU\translate('Save')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <!-- Page level custom scripts -->
    <script src="{{ asset('public/assets/select2/js/select2.min.js')}}"></script>
    <script>
        $(".js-example-theme-single").select2({
            theme: "classic"
        });

        $(".js-example-responsive").select2({
            width: 'resolve'
        });
    </script>

    <script>
        $('#add').on('click', function () {
            var name = $('#name').val();
            var symbol = $('#symbol').val();
            var code = $('#code').val();
            var exchange_rate = $('#exchange_rate').val();
            if (name == "" || symbol == "" || code == "" || exchange_rate == "") {
                alert('{{\App\CPU\translate('All input field is required')}}');
                return false;
            } else {
                return true;
            }
        });
        $(document).on('change', '.status', function () {
            var id = $(this).attr("id");
            if ($(this).prop("checked") == true) {
                var status = 1;
            } else if ($(this).prop("checked") == false) {
                var status = 0;
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{route('admin.business-settings.states.updateStatus')}}",
                method: 'POST',
                data: {
                    id: id,
                    status: status
                },
                success: function (response) {
                    if (response.status === 1) {
                        toastr.success(response.message);
                    } else {
                        toastr.error(response.message);
                    }
                    location.reload();
                }
            });
        });
    </script>
@endpush
