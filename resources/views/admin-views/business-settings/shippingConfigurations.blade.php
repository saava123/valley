
    @extends('layouts.back-end.app')

    @section('title', \App\CPU\translate('Shipping Configurations'))

    @push('css_or_js')
        <meta name="csrf-token" content="{{ csrf_token() }}">
    @endpush

    @section('content')
        <div class="content container-fluid">
{{--            <nav aria-label="breadcrumb">--}}
{{--                <ol class="breadcrumb">--}}
{{--                    <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">{{\App\CPU\translate('Dashboard')}}</a>--}}
{{--                    </li>--}}
{{--                    <li class="breadcrumb-item"--}}
{{--                        aria-current="page">{{\App\CPU\translate('seller_settings')}}</li>--}}
{{--                </ol>--}}
{{--            </nav>--}}

            <div class="aiz-main-content">
                <div class="px-15px px-lg-25px">

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="mb-0 h6">Select Shipping Method</h5>
                                </div>
                                <div class="card-body">
                                    <form action="{{route('admin.business-settings.shipping.config.update')}}" method="POST" enctype="multipart/form-data">
                                        @csrf

                                        <input type="hidden" name="type" value="shipping_type">
                                        <div class="radio mar-btm">
                                            <input id="product-shipping" class="magic-radio" type="radio"
                                                   name="shipping_type" value="product_wise_shipping"
                                            <?php if(\App\CPU\get_setting('shipping_type') === 'product_wise_shipping') echo "checked";?>
                                            >
                                            <label for="product-shipping">
                                                <span>Product Wise Shipping Cost</span>
                                                <span></span>
                                            </label>
                                        </div>
                                        <div class="radio mar-btm">
                                            <input id="flat-shipping" class="magic-radio" type="radio" name="shipping_type" value="flat_rate"
                                            <?php if(\App\CPU\get_setting('shipping_type') === 'flat_rate') echo "checked";?>>
                                            <label for="flat-shipping">Flat Rate Shipping Cost</label>
                                        </div>
                                        <div class="radio mar-btm">
                                            <input id="seller-shipping" class="magic-radio" type="radio" name="shipping_type" value="seller_wise_shipping"
                                            <?php if(\App\CPU\get_setting('shipping_type') === 'seller_wise_shipping') echo "checked";?>>
                                            <label for="seller-shipping">Seller Wise Flat Shipping Cost</label>
                                        </div>
                                        <div class="radio mar-btm">
                                            <input id="area-shipping" class="magic-radio" type="radio" name="shipping_type"
                                                   value="area_wise_shipping" <?php if(\App\CPU\get_setting('shipping_type') === 'area_wise_shipping') echo "checked";?>>
                                            <label for="area-shipping">Area Wise Flat Shipping Cost</label>
                                        </div>


                                        <div class="form-group mb-0 text-right">
                                            <button type="submit" class="btn btn-sm btn-primary">Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="mb-0 h6">Note</h5>
                                </div>
                                <div class="card-body">
                                    <ul class="list-group">
                                        <li class="list-group-item">
                                            1. Product Wise Shipping Cost calulation: Shipping cost is calculate by addition of each product shipping cost.
                                        </li>
                                        <li class="list-group-item">
                                            2. Flat Rate Shipping Cost calulation: How many products a customer purchase, doesn't matter. Shipping cost is fixed.
                                        </li>
                                        <li class="list-group-item">
                                            3. Seller Wise Flat Shipping Cost calulation: Fixed rate for each seller. If customers purchase 2 product from two seller shipping cost is calculated by addition of each seller flat shipping cost.
                                        </li>
                                        <li class="list-group-item">
                                            4. Area Wise Flat Shipping Cost calulation: Fixed rate for each area. If customers purchase multiple products from one seller shipping cost is calculated by the customer shipping area. To configure area wise shipping cost go to  <a href="http://localhost/weafmall22/admin/cities">Shipping Cities</a>.
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="mb-0 h6">Flat Rate Cost</h5>
                                </div>
                                <form action="{{route('admin.business-settings.shipping.config.update')}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="card-body">

                                        <input type="hidden" name="type" value="flat_rate_shipping_cost">
                                        <div class="form-group">
                                            <div class="col-lg-12">
                                                <input class="form-control" type="text" name="flat_rate_shipping_cost" value="{{\App\CPU\get_setting('flat_rate_shipping_cost')}}">
                                            </div>
                                        </div>
                                        <div class="form-group mb-0 text-right">
                                            <button type="submit" class="btn btn-sm btn-primary">Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="mb-0 h6">Note</h5>
                                </div>
                                <div class="card-body">
                                    <ul class="list-group">
                                        <li class="list-group-item">
                                            1. Flat rate shipping cost is applicable if Flat rate shipping is enabled.
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="mb-0 h6">Shipping Cost for Admin Products</h5>
                                </div>
                                <form action="{{route('admin.business-settings.shipping.config.update')}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="card-body">

                                        <input type="hidden" name="type" value="shipping_cost_admin">
                                        <div class="form-group">
                                            <div class="col-lg-12">
                                                <input class="form-control" type="text" name="shipping_cost_admin" value="{{\App\CPU\get_setting('shipping_cost_admin')}}">
                                            </div>
                                        </div>
                                        <div class="form-group mb-0 text-right">
                                            <button type="submit" class="btn btn-sm btn-primary">Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="mb-0 h6">Note</h5>
                                </div>
                                <div class="card-body">
                                    <ul class="list-group">
                                        <li class="list-group-item">
                                            1. Shipping cost for admin is applicable if Seller wise shipping cost is enabled.
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    @endsection

    @push('script')
        <script src="{{asset('public/assets/back-end')}}/js/tags-input.min.js"></script>
        <script src="{{ asset('public/assets/select2/js/select2.min.js')}}"></script>
        <script>
            function readWLURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#viewerWL').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#customFileUploadWL").change(function () {
                readWLURL(this);
            });

            function readWFLURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#viewerWFL').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#customFileUploadWFL").change(function () {
                readWFLURL(this);
            });

            function readMLURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#viewerML').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#customFileUploadML").change(function () {
                readMLURL(this);
            });

            function readFIURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#viewerFI').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#customFileUploadFI").change(function () {
                readFIURL(this);
            });


            $(".js-example-theme-single").select2({
                theme: "classic"
            });

            $(".js-example-responsive").select2({
                width: 'resolve'
            });

        </script>

        @include('shared-partials.image-process._script',[
            'id'=>'company-web-Logo',
            'height'=>200,
            'width'=>784,
            'multi_image'=>false,
            'route'=>route('image-upload')
            ])
        @include('shared-partials.image-process._script',[
            'id'=> 'company-footer-Logo',
            'height'=>200,
            'width'=>784,
            'multi_image'=>false,
            'route' => route('image-upload')

        ])
        @include('shared-partials.image-process._script',[
            'id'=> 'company-fav-icon',
            'height'=>100,
            'width'=>100,
            'multi_image'=>false,
            'route' => route('image-upload')

        ])
        @include('shared-partials.image-process._script',[
           'id'=>'company-mobile-Logo',
           'height'=>200,
           'width'=>784,
           'multi_image'=>false,
           'route'=>route('image-upload')
           ])

        <script>
            $(document).ready(function () {
                $('.color-var-select').select2({
                    templateResult: colorCodeSelect,
                    templateSelection: colorCodeSelect,
                    escapeMarkup: function (m) {
                        return m;
                    }
                });

                function colorCodeSelect(state) {
                    var colorCode = $(state.element).val();
                    if (!colorCode) return state.text;
                    return "<span class='color-preview' style='background-color:" + colorCode + ";'></span>" + state.text;
                }
            });
        </script>
    @endpush
