@extends('layouts.back-end.app')
@section('title', \App\CPU\translate('Attribute'))
@push('css_or_js')
    <!-- Custom styles for this page -->
    {{-- <link href="{{asset('public/assets/back-end')}}/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet"> --}}
    {{-- <link href="{{asset('public/assets/back-end/css/croppie.css')}}" rel="stylesheet"> --}}
@endpush

@section('content')
<div class="content container-fluid">
    <!-- Page Heading -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">{{ \App\CPU\translate('Dashboard')}}</a></li>
            <li class="breadcrumb-item" aria-current="page">{{ \App\CPU\translate('Attribute')}}</li>
        </ol>
    </nav>

    {{-- <!-- Content Row -->
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="flex-between justify-content-between align-items-center flex-grow-1">
                            <div>
                                <h5>{{ \App\CPU\translate('Attribute')}} {{ \App\CPU\translate('Table')}} <span style="color: red;">({{ $attributes->total() }})</span></h5>
                            </div>
                            <div style="width: 30vw">
                                <!-- Search -->
                                <form action="{{ url()->current() }}" method="GET">
                                    <div class="input-group input-group-merge input-group-flush">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="tio-search"></i>
                                            </div>
                                        </div>
                                        <input id="datatableSearch_" type="search" name="search" class="form-control"
                                            placeholder="{{\App\CPU\translate('Search_by_Attribute_Name')}}" aria-label="Search orders" value="{{ $search }}" required>
                                        <button type="submit" class="btn btn-primary">{{ \App\CPU\translate('Search')}}</button>
                                    </div>
                                </form>
                                <!-- End Search -->
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div> --}}
</div>




<div class="content container-fluid">
    <div class="aiz-titlebar text-left mt-2 mb-3">
        <div class="align-items-center">
            <h1 class="h3">{{ \App\CPU\translate('All Attributes')}}</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-7">
            <div class="card">
                <div class="card-header">
                    <h5 class="mb-0 h6">{{  \App\CPU\translate('Attributes')}}  <span style="color: red;">({{ $attributes->total() }})</span></h5>
                </div>
                <div class="card-body">
                    <table class="table aiz-table1 mb-0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>{{  \App\CPU\translate('Name')}}</th>
                                <th>{{  \App\CPU\translate('Values')}}</th>
                                <th class="text-right">{{  \App\CPU\translate('Options')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($attributes as $key=>$attribute)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$attribute['name']}}</td>
                                    <td>
                                        @foreach($attribute->attribute_values as $key => $value)
                                        <span class="badge badge-inline badge-md bg-soft-dark">{{ $value->value }}</span>
                                        @endforeach
                                    </td>
                                    <td class="text-right" >
                                        {{-- <a class="btn btn-soft-info btn-icon btn-circle btn-sm" href="{{route('attributes.show', $attribute->id)}}" title="{{  \App\CPU\translate('Attribute values') }}">
                                            <i class="las la-cog"></i>
                                        </a> --}}
                                        {{-- <a class="btn btn-soft-primary btn-icon btn-circle btn-sm" href="{{route('admin.attribute.edit',[$attribute['id']])}}" title="{{  \App\CPU\translate('Edit') }}">
                                            <i class="las la-edit"></i>
                                        </a>
                                        <a href="#" class="btn btn-soft-danger btn-icon btn-circle btn-sm confirm-delete" data-href="{{$attribute['id']}}" title="{{  \App\CPU\translate('Delete') }}">
                                            <i class="las la-trash"></i>
                                        </a> --}}

                                        <div class="dropdown">
                                            <button class="btn btn-outline-secondary dropdown-toggle" type="button"
                                                    id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                                    aria-expanded="false">
                                                <i class="tio-settings"></i>
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="btn btn-soft-primary btn-icon btn-circle btn-sm" href="{{route('admin.attribute.edit',[$attribute['id']])}}" title="{{  \App\CPU\translate('Edit') }}">
                                                    <i class="las la-edit"></i>
                                                </a>
                                                <a href="#" class="btn btn-soft-danger btn-icon btn-circle btn-sm confirm-delete" data-href="{{$attribute['id']}}" title="{{  \App\CPU\translate('Delete') }}">
                                                    <i class="las la-trash"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    {!! $attributes->links() !!}
                </div>
                @if(count($attributes)==0)
                        <div class="text-center p-4">
                            <img class="mb-3" src="{{asset('public/assets/back-end')}}/svg/illustrations/sorry.svg" alt="Image Description" style="width: 7rem;">
                            <p class="mb-0">{{ \App\CPU\translate('No_data_to_show')}}</p>
                        </div>
                    @endif
            </div>
        </div>
        <div class="col-md-5">
            <div class="card">
                <div class="card-header">
                        <h5 class="mb-0 h6">{{  \App\CPU\translate('Add New Attribute') }}</h5>
                </div>
                <div class="card-body">
                    <form action="{{ route('admin.attribute.store') }}" method="POST">
                        @csrf
                        <div class="form-group mb-3">
                            <label for="name">{{ \App\CPU\translate('Name')}}</label>
                            <input type="text" placeholder="{{  \App\CPU\translate('Name')}}" id="name" name="name" class="form-control" required>
                        </div>
                        <div class="form-group mb-3 text-right">
                            <button type="submit" class="btn btn-primary">{{ \App\CPU\translate('Save')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section('modal')
    @include('admin-views.modals.delete')
@endsection

@push('script')
<script>
        // $(".lang_link").click(function (e) {
        //     e.preventDefault();
        //     $(".lang_link").removeClass('active');
        //     $(".lang_form").addClass('d-none');
        //     $(this).addClass('active');

        //     let form_id = this.id;
        //     let lang = form_id.split("-")[0];
        //     console.log(lang);
        //     $("#" + lang + "-form").removeClass('d-none');
        //     if (lang == 'default_lang}}') {
        //         $(".from_part_2").removeClass('d-none');
        //     } else {
        //         $(".from_part_2").addClass('d-none');
        //     }
        // });

        $(document).ready(function () {
            $('#dataTable').DataTable();
        });
    </script>
    <script>


        $(document).on('click', '.delete', function () {
            var id = $(this).attr("id");
            Swal.fire({
                title: '{{\App\CPU\translate('Are_you_sure_to_delete_this')}}?',
                text: "{{\App\CPU\translate('You_will not_be_able_to_revert_this')}}!",
                showCancelButton: true,
                confirmButtonColor: 'primary',
                cancelButtonColor: 'secondary',
                confirmButtonText: '{{\App\CPU\translate('Yes')}}, {{\App\CPU\translate('delete_it')}}!'
            }).then((result) => {
                if (result.value) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: "{{route('admin.attribute.delete')}}",
                        method: 'POST',
                        data: {id: id},
                        success: function () {
                            toastr.success('{{\App\CPU\translate('Attribute_deleted_successfully')}}');
                            location.reload();
                        }
                    });
                }
            })
        });



         // Call the dataTables jQuery plugin


    </script>
@endpush
