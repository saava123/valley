<div class="col-lg-3 position-static d-none d-lg-block " style="">
    <div class="aiz-category-menu bg-white rounded h-lg-450px shadow-sm">
        {{--                    <div class="p-1 d-none d-lg-block rounded-top all-category position-relative text-left" style="background-color: #3a3a44; color: #fff;">--}}
        {{--                        <span class="fw-600 fs-16 m-3">Categories</span>--}}
        {{--                        <a href="https://weafmall.com/categories" class="text-reset">--}}
        {{--                            <span class="d-none d-lg-inline-block float-right mr-1"> <i class="las la-angle-right la-2x "></i></span>--}}
        {{--                        </a>--}}
        {{--                    </div>--}}
        @if(request()->is('/'))
            @foreach($categories as $key => $category)
                @if($key<11)
        <ul class="list-unstyled categories no-scrollbar mb-0 text-left">
            <li class="category-nav-element" data-id="{{$category['id']}}">
                <a href="{{route('products',['id'=> $category['id'],'data_from'=>'category','page'=>1])}}"
                   class="text-truncate text-reset d-block">
                    <img class="cat-image mr-2 opacity-60 lazyloaded" src="{{asset("storage/app/public/category/$category->icon")}}"
                         data-src="{{asset('public/assets/front-end/img/image-place-holder.png')}}" width="16" alt="{{$category->name}}"
                         onerror="this.onerror=null;this.src='{{asset('public/assets/front-end/img/image-place-holder.png')}}';">
                    <span class="cat-name text-dark">{{$category->name}}</span>
                </a>

                @if(count(\App\Utility\CategoryUtility::get_immediate_children_ids($category->id))>0)
                    <div class="sub-cat-menu c-scrollbar-light rounded shadow-lg p-4 text-dark">
                        <div class="c-preloader text-center absolute-center">
                            <i class="fa fa-spinner fa-x6 opacity-70"></i>
                        </div>
                    </div>
                @endif
            </li>
        </ul>
            @endif
                    @endforeach
            @endif
    </div>
</div>
