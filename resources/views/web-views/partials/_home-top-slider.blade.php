
<div class="home-banner-area mb-4 pt-3">
    <div class="container">
        <div class="row gutters-10 position-relative">
              @include('web-views.partials._category_menu')


            <div class=" col-lg-7 col-md-12">

                @php($main_banner=\App\Model\Banner::where('banner_type','Main Banner')->where('published',1)->orderBy('id','desc')->get())
                <div id="carouselExampleIndicators" class="carousel slide owl-loading owl-dot" data-ride="carousel">
                    <ol class="carousel-indicators owl-dots">
                        @foreach($main_banner as $key=>$banner)
                            <li data-target="#carouselExampleIndicators" data-slide-to="{{$key}}"
                                class="{{$key==0?'active':''}}">
                            </li>
                        @endforeach
                    </ol>
                    <div class="carousel-inner">
                        @foreach($main_banner as $key=>$banner)
                            <div class="carousel-item {{$key==0?'active':''}}">
                                <a href="{{$banner['url']}}">
                                    <img class="d-block w-100" style="max-height: 350px"
                                         onerror="this.src='{{asset('public/assets/front-end/img/image-place-holder.png')}}'"
                                         src="{{asset('storage/app/public/banner')}}/{{$banner['photo']}}"
                                         alt="">
                                </a>
                            </div>
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                       data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">{{\App\CPU\translate('Previous')}}</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                       data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">{{\App\CPU\translate('Next')}}</span>
                    </a>
                </div>
<div class="ranking-main">
    <div class="_3b77i">
        <div class="_1klqZ" style="background: rgb(255, 245, 245) url(&quot;https://ae01.alicdn.com/kf/H863bb19ae5de49a8bd6074a3d81599d9z.jpg_Q90.jpg_.webp&quot;) no-repeat scroll 0px 0px / cover;">
            <div class="_1r8bG">
                <div class="_17Zae">
                    <h3 class="_2Z33F">Welcome newcomers!</h3>
                    <p class="_3yqUR">Get items for UGX 2000 or get a UGX 8000 coupon!</p>
                </div>
                <div class="_1pzk6" style="background-position: 0px 0px;
                            background-repeat: no-repeat; background-attachment: scroll;
                            background-image: url(&quot;https://ae01.alicdn.com/kf/Hc9b782577fb54b369d5aface2f69b38cD.gif&quot;);
                            background-size: contain; background-origin: padding-box; background-clip: border-box;">
                    <h3 class="_1OU-S">UGX 36000</h3>
                    <p class="_2UxGq">Claim</p>
                </div>
            </div>
            <div class="wGiim">
                <div class="_2bRWn">
                    <div class="_2EB3t">
                        <div class="slick-slider slick-initialized"  dir="ltr" style="height: 145px;">
{{--                            <button type="button" data-role="none" class="slick-arrow slick-prev slick-disabled" --}}
{{--                                    style="display: block;"> Previous</button>--}}
                            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                               data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">{{\App\CPU\translate('Previous')}}</span>
                            </a>
{{--                            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"--}}
{{--                               data-slide="next">--}}
{{--                                <span class="carousel-control-next-icon" aria-hidden="true"></span>--}}
{{--                                <span class="sr-only">{{\App\CPU\translate('Next')}}</span>--}}
{{--                            </a>--}}
                            <div class="slick-list owl-carousel owl-theme" id="new-slider" style="height: 149px;">
                                <div class="slick-track "  style="width: 1017px; opacity: 1; transform: translate3d(0px, 0px, 0px);">
                                    <div data-index="0" class="slick-slide slick-active slick-current" tabindex="-1" aria-hidden="false"
                                         style="outline: currentcolor none medium; width: 113px;">
                                        <div>
                                            <div class="KGMPS" style="width: 100%; display: inline-block;">
                                                <div class="_1S5TX">
                                                    <div class="EfdcE">
                                                        <div class="_3XCuR wrUT0">
                                                            <img src="//ae04.alicdn.com/kf/Ha5efc852bd0a416ba228ef6e2477d02cn.jpg_120x120Q90.jpg_.webp">
                                                        </div>
                                                    </div>
                                                    <span class="_23vXL">UGX 100</span>
                                                    <span class="_1N9Dk">UGX 7850</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div data-index="1" class="slick-slide slick-active" tabindex="-1" aria-hidden="false"
                                         style="outline: currentcolor none medium; width: 113px;">
                                        <div>
                                            <div class="KGMPS" style="width: 100%; display: inline-block;">
                                                <div class="_1S5TX">
                                                    <div class="EfdcE">
                                                        <div class="_3XCuR wrUT0">
                                                            <img src="//ae04.alicdn.com/kf/H867d729e0bc047b291ee41f78414e64aN.jpg_120x120Q90.jpg_.webp">
                                                        </div>
                                                    </div>
                                                    <span class="_23vXL">UGX 1.00</span><span class="_1N9Dk">UGX 178.00</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div data-index="2" class="slick-slide slick-active" tabindex="-1" aria-hidden="false"
                                         style="outline: currentcolor none medium; width: 113px;">
                                        <div>
                                            <div class="KGMPS" style="width: 100%; display: inline-block;">
                                                <div class="_1S5TX">
                                                    <div class="EfdcE">
                                                        <div class="_3XCuR wrUT0">
                                                            <img src="//ae04.alicdn.com/kf/Hbf53f700b71c4ae9bb225cb86f507fc8W.jpg_120x120Q90.jpg_.webp">
                                                        </div>
                                                    </div><span class="_23vXL">UGX 2.00</span><span class="_1N9Dk">UGX 477.00</span></div></div></div></div><div data-index="3" class="slick-slide slick-active" tabindex="-1" aria-hidden="false" style="outline: currentcolor none medium; width: 113px;"><div><div class="KGMPS" style="width: 100%; display: inline-block;"><div class="_1S5TX"><div class="EfdcE"><div class="_3XCuR wrUT0"><img src="//ae04.alicdn.com/kf/H1458d30b8f984501858833b0504e5583P.jpg_120x120Q90.jpg_.webp"></div></div><span class="_23vXL">UGX 2.00</span><span class="_1N9Dk">UGX 771.00</span></div></div></div></div><div data-index="4" class="slick-slide" tabindex="-1" aria-hidden="true" style="outline: currentcolor none medium; width: 113px;"><div><div class="KGMPS" style="width: 100%; display: inline-block;"><div class="_1S5TX"><div class="EfdcE"><div class="_3XCuR wrUT0"><img src="//ae04.alicdn.com/kf/HTB1.gazbLWG3KVjSZFgq6zTspXa2.jpg_350x350.jpg_.webp"></div></div><span class="_23vXL">UGX 1.00</span><span class="_1N9Dk">UGX 488.00</span></div></div></div></div><div data-index="5" class="slick-slide" tabindex="-1" aria-hidden="true" style="outline: currentcolor none medium; width: 113px;"><div><div class="KGMPS" style="width: 100%; display: inline-block;"><div class="_1S5TX"><div class="EfdcE"><div class="_3XCuR wrUT0"><img src="//ae04.alicdn.com/kf/H54273356bebe48699ed745869cdc5593v.jpg_120x120Q90.jpg_.webp"></div></div><span class="_23vXL">UGX 2.00</span><span class="_1N9Dk">UGX 879.00</span></div></div></div></div><div data-index="6" class="slick-slide" tabindex="-1" aria-hidden="true" style="outline: currentcolor none medium; width: 113px;"><div><div class="KGMPS" style="width: 100%; display: inline-block;"><div class="_1S5TX"><div class="EfdcE"><div class="_3XCuR wrUT0"><img src="//ae04.alicdn.com/kf/Hb29005345ada405ea3329ea65949c75do.jpg_120x120Q90.jpg_.webp"></div></div><span class="_23vXL">UGX 2.00</span><span class="_1N9Dk">UGX 950.00</span></div></div></div></div><div data-index="7" class="slick-slide" tabindex="-1" aria-hidden="true" style="outline: currentcolor none medium; width: 113px;"><div><div class="KGMPS" style="width: 100%; display: inline-block;"><div class="_1S5TX"><div class="EfdcE"><div class="_3XCuR wrUT0"><img src="//ae04.alicdn.com/kf/H1b6f2aa343ad4fce8d923d855f62e7195.jpg_120x120Q90.jpg_.webp"></div></div><span class="_23vXL">UGX 1.00</span><span class="_1N9Dk">UGX 658.00</span></div></div></div></div><div data-index="8" class="slick-slide" tabindex="-1" aria-hidden="true" style="outline: currentcolor none medium; width: 113px;"><div><div class="KGMPS" style="width: 100%; display: inline-block;"><div class="_1S5TX"><div class="EfdcE"><div class="_3XCuR wrUT0"><img src="//ae04.alicdn.com/kf/H38ea1741e1544878a3c073741a833da2S.jpg_120x120Q90.jpg_.webp">
                                                        </div>
                                                    </div>
                                                    <span class="_23vXL">UGX 1.00</span><span class="_1N9Dk">UGX 229.00</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
{{--                            <button type="button" data-role="none" class="slick-arrow slick-next" style="display: block;"> Next</button>--}}
                        </div></div></div></div></div></div>
</div>

            </div>


            <div class="col-lg-2 order-3 mt-3 mt-lg-0  d-none d-md-block d-md-none d-lg-block">
                <div class="bg-white rounded shadow-sm">
{{--                    <div class=" rounded-top p-1 d-flex align-items-center justify-content-center" style="background-color: #3a3a44; color: #fff;">--}}
{{--                            <span class="fw-600 fs-16 mr-2 text-truncate">--}}
{{--                                Todays Deal--}}
{{--                            </span>--}}
{{--                        <span class="badge badge-primary badge-inline">Hot</span>--}}
{{--                    </div>--}}
                    <div class=" h-lg-450px  rounded">
                        <div class="gutters-5 lg-no-gutters row row-cols-2 row-cols-lg-1">
                            <div class="col">
                                <div class="main-mt-login">
                                    <div class="info-main"><span><a href="#">
                                                <img src="https://s.staticbg.com/web/src/img/indexnewest/default.png"
                                                     data-src="//s.staticbg.com/web/src/img/indexnewest/default.png">
                                            </a>
                                        </span>
                                        <strong>
                                            <a href="#">Welcome to eShop</a>
                                        </strong></div><div class="info-btn">
                                        <a href="#" dpid="top_StartHere_button_180104|home|182202736">Join us</a>
                                        <a href="#" dpid="top_SignIn_button_180104|home|182202735">Sign in</a>
                                    </div><dl><dt>Customer Service Policy</dt>
                                        <dd><i class="iconfont icon-a-PaymentSecurity"></i>Payment Security</dd>
                                        <dd><i class="iconfont icon-a-DeliveryGuarantee"></i>Delivery Guarantee</dd>
                                        <dd><i class="iconfont icon-a-QualityGuarantee"></i>
                                            Quality Guarantee</dd><dd>
                                            <i class="iconfont icon-a-NoReasonReturns"></i>No Reason Returns</dd>
                                    </dl><span class="policy-see">
                                        <a href="#"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 dpid="homeServicePolicy_see more_button_211021|home|21293013756">See More</a></span><span class="policy-bg"></span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="row mt-2 bg-white shadow-sm rounded p-2">

        @foreach(\App\Model\Banner::where('banner_type','Footer Banner')->where('published',1)->orderBy('id','desc')->take(3)->get() as $banner)
            <div class="col-4">
                <a data-toggle="modal" data-target="#quick_banner{{$banner->id}}"
                   style="cursor: pointer;">
                    <img class="d-block footer_banner_img" style="width: 100%; border-radius: 4px;"
                         onerror="this.src='{{asset('public/assets/front-end/img/image-place-holder.png')}}'"
                         src="{{asset('storage/app/public/banner')}}/{{$banner['photo']}}">
                </a>
            </div>
            <div class="modal fade" id="quick_banner{{$banner->id}}" tabindex="-1"
                 role="dialog" aria-labelledby="exampleModalLongTitle"
                 aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <p class="modal-title"
                               id="exampleModalLongTitle">{{ \App\CPU\translate('banner_photo')}}</p>
                            <button type="button" class="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <img class="d-block mx-auto"
                                 onerror="this.src='{{asset('public/assets/front-end/img/image-place-holder.png')}}'"
                                 src="{{asset('storage/app/public/banner')}}/{{$banner['photo']}}">
                            @if ($banner->url!="")
                                <div class="text-center mt-2">
                                    <a href="{{$banner->url}}"
                                       class="btn btn-outline-accent">{{\App\CPU\translate('Explore')}} {{\App\CPU\translate('Now')}}</a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
