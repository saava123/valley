<?php

namespace App\Models;

use App\Model\Seller;
use Illuminate\Database\Eloquent\Model;

class SellerPackagePayment extends Model
{
    public function user(){
        return $this->belongsTo(Seller::class);
    }

    public function seller_package(){
        return $this->belongsTo(SellerPackage::class);
    }
}
