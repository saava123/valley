<?php

namespace App\Http\Controllers;

use App\Model\Category;
use Illuminate\Http\Request;

class Home2Controller extends Controller
{
    public function get_category_items(Request $request){
        $category = Category::findOrFail($request->id);
//        dd($category);
        return view('layouts.front-end.partials.category_elements', compact('category'));
    }
}
