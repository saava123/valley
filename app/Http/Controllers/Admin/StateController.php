<?php

namespace App\Http\Controllers\Admin;

use App\CPU\Helpers;
use App\Http\Controllers\Controller;
use App\Model\Country;
use App\Model\State;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;

class StateController extends Controller
{
    public function index(Request $request)
    {
        $sort_country = $request->sort_country;
        $sort_state = $request->sort_state;

        $state_queries = State::query();
        if ($request->sort_state) {
            $state_queries->where('name', 'like', "%$sort_state%");
        }
        if ($request->sort_country) {
            $state_queries->where('country_id', $request->sort_country);
        }

        $states = $state_queries->orderBy('status', 'desc')->paginate(15);
//        dd($states);

//        $countries = $states->latest()->paginate(Helpers::pagination_limit())->appends($query_param);
        return view('admin-views.business-settings.state.index', compact('states', 'sort_country', 'sort_state'));
    }

    public function store(Request $request)
    {
        $state = new State;

        $state->name        = $request->name;
        $state->country_id  = $request->country_id;
        $state->save();

        Toastr::success(' State has been inserted successfully');
        return back();
    }

    public function edit($id)
    {
        $state  = State::findOrFail($id);
        $countries = Country::where('status', 1)->get();

        return view('admin-views.business-settings.state.edit', compact('countries', 'state'));
    }

    public function update(Request $request, $id)
    {
        $state = State::findOrFail($id);

        $state->name        = $request->name;
        $state->country_id  = $request->country_id;

        $state->save();

        flash(translate('State has been updated successfully'))->success();
        return back();
    }

    public function destroy($id)
    {
        State::destroy($id);

        flash(translate('State has been deleted successfully'))->success();
        return redirect()->route('states.index');
    }

    public function updateStatus(Request $request)
    {
        $state = State::findOrFail($request->id);
        $state->status = $request->status;
        $state->save();

        if ($state->status) {
            foreach ($state->cities as $city) {
                $city->status = 1;
                $city->save();
            }
        }

        return 1;
    }
}
